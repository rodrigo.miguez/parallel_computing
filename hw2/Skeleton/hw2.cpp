/***************************************************************************************************
Name        : hw2.cpp
Description : This code is designed for educational purposes.
              - Implement one-sided MPI communication
                - MPI_Get
                - MPI_Accumulate
***************************************************************************************************/

#include <mpi.h>
#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

void localizeArray(int npes, int nnc, int cSize0, int cSize1, double* localArray,
                   double* globalArray) {

  MPI_Win win0;
  MPI_Win_create(localArray, nnc*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD,
                 &win0);
  MPI_Win_fence(0, win0);

  for (int i=0; i < npes; i++) {
    if (i != npes-1) {
      MPI_Get(&globalArray[i * cSize0], cSize0, MPI_DOUBLE, i, 0, cSize0, MPI_DOUBLE, win0);
    }
    else {
      MPI_Get(&globalArray[i * cSize0], cSize1, MPI_DOUBLE, i, 0, cSize1, MPI_DOUBLE, win0);
    }
  }

  MPI_Win_fence(0, win0);
  MPI_Win_free(&win0);

}

void accumulateArray(int npes, int nnc, int cSize0, int cSize1, double * localArray,
                     double * globalArray, double * sumArray) {

  MPI_Win win1;
  MPI_Win_create(sumArray, nnc*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD,
                 &win1);
  MPI_Win_fence(0, win1);

  for (int i=0; i < npes; i++) {
    if (i != npes-1) {
      MPI_Accumulate(localArray, cSize0, MPI_DOUBLE, i, 0, cSize0, MPI_DOUBLE, MPI_SUM, win1);
    }
    else {
      MPI_Accumulate(localArray, cSize1, MPI_DOUBLE, i, 0, cSize1, MPI_DOUBLE, MPI_SUM, win1);
    }
  }

  MPI_Win_fence(0, win1);
  MPI_Win_free(&win1);

}

double checkSum(int length, double* array)
{

  double sum = 0;
  for(int i = 0; i < length; i++) {
    sum += array[i];
  }

  return sum;
}

void displayResults(double* localArray, double* globalArray, double* sumArray, int nn, int nnc, int mype)
{
    cout << "##################################################################" << endl;
    cout << endl;
    cout << "mype:" << mype << " nnc:" << nnc << endl;
    cout << "mype: " << mype << " localArray[ ";
    for(int i=0; i<nnc; i++){
        cout << localArray[i] << " " ;
    }
    cout << " ]" << endl;

    cout << "mype: " << mype << " globalArray[ ";
    for(int i=0; i<nn; i++)
    {
        cout << globalArray[i] << " ";
    }
    cout << "]" << endl;

    cout << "mype: " << mype << " sumArray[ ";
    for(int i=0; i<nnc; i++)
    {
        cout << sumArray[i] << " ";
    }
    cout << "]" << endl;

    cout << endl;
    cout << "mype: " << mype << " checkSum globalArray: " << checkSum(nn, globalArray) << endl;
    cout << "mype: " << mype << " checkSum sumArray: " << checkSum(nnc, sumArray) << endl;
    cout << endl;

}

int main(int argc, char **argv) {
/***************************************************************************************************
MAIN PROGRAM FLOW
1. Initialize the data
2. MPI_Get
3. MPI_Accumulate
4. Display results
***************************************************************************************************/

    int mype, npes;
    double starttime;
    int nn, nnc, mnc;
    double * localArray;
    double * globalArray;
    double * sumArray;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &mype);
    MPI_Comm_size(MPI_COMM_WORLD, &npes);

    // 1. Define lenght of the main vector
    nn = 9600;

    // 2. Determine nnc, mnc
    int cSize0, cSize1;
    if ( ( nn % npes ) == 0 ) {
      nnc = nn / npes;
      cSize0 = nnc;
      cSize1 = nnc;
    }
    else {
      nnc = ( nn / npes ) + 1;
      cSize0 = nnc;
      cSize1 = nn - ( nnc * (npes-1) );
      if ( mype == npes-1 ) {
        nnc = nn - ( nnc * (npes-1) );
      }
    }

    // 3. Create localArray, globalArray and sumArray
    localArray = (double *) malloc (nnc* (sizeof(double)));
    sumArray =  (double *) malloc (nnc* (sizeof(double)));
    for (int i=0; i < nnc; i++) {
      localArray[i] = 0.;
      sumArray[i] = 0;
    }

    globalArray = (double *) malloc (nn* (sizeof(double)));
    for (int i=0; i < nn; i++) {
      globalArray[i] = 0;
    }

    // 4. Initialize the data in the different array.
    // localArray has to be initialized with random values between 0 and 10.
    srand(time(NULL) + mype);
    for (int i=0; i < nnc; i++) {
      localArray[i] = ((double)rand()/(double)RAND_MAX)*10.;
    }

    // 5. Transfer data from remote to local (MPI_Get)
    localizeArray(npes, nnc, cSize0, cSize1, localArray, globalArray);

    // 6. Accumulate data from local to remote (MPI_Accumulate)
    accumulateArray(npes, nnc, cSize0, cSize1, localArray, globalArray, sumArray);

    // 7. Display the resulting arrays together with their checksum
    MPI_Barrier(MPI_COMM_WORLD);
    for(int i = 0; i < npes; i++) {
        MPI_Barrier(MPI_COMM_WORLD);
        if (i == mype) {
            displayResults(localArray, globalArray, sumArray, nn, nnc, mype);
        }
    }

    MPI_Finalize();

    return 0;
}
