\documentclass[a4paper, 11pt, oneside]{scrartcl}
\usepackage{lpp}
\usepackage{verbatim}
\usepackage[section]{placeins}

\begin{document}

\mtitle{Homework 2}

\sauthor{Rodrigo Barros Miguez}

\maff{Laboratory for Parallel Programming}{rodrigo.miguez@rwth-aachen.de}{}

\section*{Introduction}

When dealing with parallel architectures, the concept of distributed memory systems comes up as an important way of obtaining increasing computational performance. In clusters such as Jureca super computer from the Forschungszentrum Juelich and the RWTH cluster, many nodes are combined to achieve high computer performance. This combination of nodes leads to a distributed architecture where MPI is one of the most widely used libraries to parallelize code in such environments.\\
In this homework, the MPI library is used to perform an one-sided communication. The problem consists of an array of arbitrary length that should be divided into chunks of approximately similar size between the number of ranks used. First, the array length was set constant and the number of ranks was increased to observe the relationship between communication and computation of the code. Then, the array length was increased and maintaining the number of ranks constant. The analysis of the timings was performed using the software vampire.  

\section*{Implementation and Results}

The code shown below represents the implementation of an one-sided communication using the MPI library. First, the length of a vector is defined manually in the code. Then, the array is the array is divided into chunks according to the array length and the number of ranks used for the run. In the next steps the local, global and sum arrays are allocated and initialized where, localArray is initialized with random floating numbers between 0 and 10. Finally, the communitation takes place using the functions localizeArray and accumulateArray which uses the \texttt{MPI\_Get} and \texttt{MPI\_Accumulate}, respectively.

\begin{verbatim}

  ...

  // 1. Define lenght of the main vector
  nn = 9600;

  // 2. Determine nnc, mnc
  int cSize0, cSize1;
  if ( ( nn % npes ) == 0 ) {
    nnc = nn / npes;
    cSize0 = nnc;
    cSize1 = nnc;
  }
  else {
    nnc = ( nn / npes ) + 1;
    cSize0 = nnc;
    cSize1 = nn - ( nnc * (npes-1) );
    if ( mype == npes-1 ) {
      nnc = nn - ( nnc * (npes-1) );
    }
  }

  // 3. Create localArray, globalArray and sumArray
  localArray = (double *) malloc (nnc* (sizeof(double)));
  sumArray =  (double *) malloc (nnc* (sizeof(double)));
  for (int i=0; i < nnc; i++) {
    localArray[i] = 0.;
    sumArray[i] = 0;
  }

  globalArray = (double *) malloc (nn* (sizeof(double)));
  for (int i=0; i < nn; i++) {
    globalArray[i] = 0;
  }

  // 4. Initialize the data in the different array.
  // localArray has to be initialized with random values between 0 and 10.
  srand(time(NULL) + mype);
  for (int i=0; i < nnc; i++) {
    localArray[i] = ((double)rand()/(double)RAND_MAX)*10.;
  }

  // 5. Transfer data from remote to local (MPI_Get)
  localizeArray(npes, nnc, cSize0, cSize1, localArray, globalArray);

  // 6. Accumulate data from local to remote (MPI_Accumulate)
  accumulateArray(npes, nnc, cSize0, cSize1, localArray, globalArray, sumArray);

  ...
\end{verbatim}

To perform the \texttt{MPI\_Get} operation, first a window is created by the ranks so the one-sided communication can then take place. After that, a loop is performed to get the data in the localArray of each rank to the rank calling the \texttt{MPI\_Get}. It is important to notice that the length of the chunks distributed over the ranks might not be the same, i.e., if the array cannot be evenly divided then the last rank will get a different chunk size. Thus, the loop uses a different pattern for the last rank.\\
The same logic is followed when implementing the \texttt{MPI\_Accumulate} as is presented below.

\begin{verbatim}

  ...
  
  void localizeArray(int npes, int nnc, int cSize0, int cSize1, double* localArray,
                     double* globalArray) {

  MPI_Win win0;
  MPI_Win_create(localArray, nnc*sizeof(double), sizeof(double), MPI_INFO_NULL,
                 MPI_COMM_WORLD, &win0);
  MPI_Win_fence(0, win0);

  for (int i=0; i < npes; i++) {
    if (i != npes-1) {
      MPI_Get(&globalArray[i * cSize0], cSize0, MPI_DOUBLE, i, 0, cSize0, MPI_DOUBLE,
              win0);
    }
    else {
      MPI_Get(&globalArray[i * cSize0], cSize1, MPI_DOUBLE, i, 0, cSize1, MPI_DOUBLE,
              win0);
    }
  }

  MPI_Win_fence(0, win0);
  MPI_Win_free(&win0);

  }

  void accumulateArray(int npes, int nnc, int cSize0, int cSize1, double * localArray,
                       double * globalArray, double * sumArray) {

  MPI_Win win1;
  MPI_Win_create(sumArray, nnc*sizeof(double), sizeof(double), MPI_INFO_NULL, 
                 MPI_COMM_WORLD,  &win1);
  MPI_Win_fence(0, win1);

  for (int i=0; i < npes; i++) {
    if (i != npes-1) {
      MPI_Accumulate(localArray, cSize0, MPI_DOUBLE, i, 0, cSize0, MPI_DOUBLE, MPI_SUM,
                     win1);
    }
    else {
      MPI_Accumulate(localArray, cSize1, MPI_DOUBLE, i, 0, cSize1, MPI_DOUBLE, MPI_SUM,
                     win1);
    }
  }

  MPI_Win_fence(0, win1);
  MPI_Win_free(&win1);

  }

  ...
\end{verbatim}

To perform the runs using different number of PEs, an array size of nn = 64 was used. Then the following PEs numbers were adopted: 2, 4, 8, 16, and 32. Figures 1 to 5 depicted below show the communication pattern for each run. When comparing the plots it is possible to observe that the cost of the MPI communication increases when increasing the number of PEs used and the total computation time also increases. Additionally, the gap between the time needed for communication and computations grows larger as the number of PEs is increased.

\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{../images/vampir_2_64.png}
\caption{Communication pattern PEs = 2, nn = 64.}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{../images/vampir_4_64.png}
\caption{Communication pattern PEs = 4, nn = 64.}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{../images/vampir_8_64.png}
	\caption{Communication pattern PEs = 8, nn = 64.}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{../images/vampir_16_64.png}
	\caption{Communication pattern PEs = 16, nn = 64.}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{../images/vampir_32_64.png}
	\caption{Communication pattern PEs = 32, nn = 64.}
\end{figure}

Finally, a run using an array of length nn = 9600 and a PE number equal to 32 was performed. This way the chunk size obtained when splitting the array among the ranks reaches 300. Figure 6 shows the communication pattern of this run where it is observed that the timing needed for computation has been increased due to the larger size of the array but the timing needed for communication did not increase as much in comparison to the run using 32 PEs and nn equal to 64. This shows a better balance between computation and communication.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{../images/vampir_32_nn9600.png}
	\caption{Communication pattern PEs = 32, nn = 9600.}
\end{figure}

\section*{Conclusion}

In this homework the MPI library was used to perform an one-sided communication exercise of the division of an array between ranks followed by a summation of the localArrays. It was observed that for the length of the array used, the communication timing was superior if compared to the computation. This difference increased as the number of PEs was increased while maintaining the array length constant. Additionally, it was observed that when the array length is increased, the computation time can increase drastically while the communication timing do not increase much if compared to the case with same PEs number and smaller array size.

\bibliographystyle{}
\bibliography{}

\end{document}
