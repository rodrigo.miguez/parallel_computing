#!/usr/bin/env zsh

### Job name
#BSUB -J para-pr2

### Job output (use %J  for job ID)
#BSUB -o para-pr2.%J.qout

### Request the time you need for execution in minutes
#BSUB -W 0:30

# Use the job queue that has been created for this particular lecture
#BSUB -Plect0027

### Request memory you need for your job in TOTAL in MB
#BSUB -M 512

### Use nodes exclusive
#BSUB -x

### Request the number of compute slots you want to use
#BSUB -n 1

### Use esub for OpenMP/shared memory jobs
#BSUB -a openmp

### Change to the work directory
cd test

### Execute your application
../build/2d_Unsteady_OpenMP
