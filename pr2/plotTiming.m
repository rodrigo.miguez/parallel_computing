clear all, clc
close all

serial = 51;
reduction = [29; 41; 22; 24];
atomic = [108; 80; 72; 96];

figure(1)
plot([2 4 8 12],reduction,'-b');
xlabel('Number of compute slots');
ylabel('Run time');
title('Reduction')

figure(3)
plot([2 4 8 12],atomic,'-b');
xlabel('Number of compute slots');
ylabel('Run time');
title('Atomic')
