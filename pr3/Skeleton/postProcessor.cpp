#include "postProcessor.h"

/***************************************************************************************************
preProcessorControl
**************************************************************************************************/

void postProcessor::postProcessorControl(inputSettings* argSettings, triMesh* argMesh)
{
    int mype, npes;             // my processor rank and total number of processors
    MPI_Comm_rank(MPI_COMM_WORLD, &mype);
    MPI_Comm_size(MPI_COMM_WORLD, &npes);
    mesh = argMesh;
    settings = argSettings;

    if (mype==0) cout << endl << "================ POST-PROCESSING =================" << endl;

 // evaluateLimits();
 vtkVisualization();
    return;
}

/***************************************************************************************************
Evaluates the maximum and minimum temperatures in the field
***************************************************************************************************/
void postProcessor::evaluateLimits()
{
    int nn = mesh->getNn();
    int nnc = mesh->getNnc();
    int mype, npes;             // my processor rank and total number of processors
    MPI_Comm_rank(MPI_COMM_WORLD, &mype);
    MPI_Comm_size(MPI_COMM_WORLD, &npes);
    double T;
    minT = std::numeric_limits<double>::max();
    maxT = std::numeric_limits<double>::min();

    for(int i=0; i<nnc; i++)
    {
    T = mesh->getNode(i)->getT();
        if(T < minT)
            minT = T;
        if(T > maxT)
            maxT = T;
    }
//  cout << "Tmin " << minT << endl << "Tmax " << maxT << endl;

    return;
}

/***************************************************************************************************
// Main visualization function
***************************************************************************************************/
void postProcessor::vtkVisualization()
{
    int nn = mesh->getNn();
    int nnc = mesh->getNnc();
    int nnl = mesh->getNnl();
    int mnc = mesh->getMnc();
    int ne = mesh->getNe();
    int nec = mesh->getNec();
    int mec = mesh->getMec();
    double * TL = mesh->getTL();

     int mype, npes;             // my processor rank and total number of processors

     MPI_Comm_rank(MPI_COMM_WORLD, &mype);
     MPI_Comm_size(MPI_COMM_WORLD, &npes);
     ostringstream int2str;
     int2str << mype;
     string strMype = int2str.str();
     string dummy;

     // VTK Double Array
     vtkSmartPointer<vtkDoubleArray> pcoords = vtkSmartPointer<vtkDoubleArray>::New();
     pcoords->SetNumberOfComponents(nen);
     pcoords->SetNumberOfTuples(nnl);

     //vtkDoubleArray type pcoords is filled with the data in meshPoints.
     for (int i=0; i<nnl; i++)
         pcoords->SetTuple3(i,mesh->getLNode(i)->getX(),mesh->getLNode(i)->getY(),0.0f);

     //vtkPoints type outputPoints is filled with the data in pcoords.
     vtkSmartPointer<vtkPoints> outputPoints = vtkSmartPointer<vtkPoints>::New();
     outputPoints->SetData(pcoords);

     //Connectivity is written to vtkCellArray type outputCells
     vtkSmartPointer<vtkCellArray> connectivity = vtkSmartPointer<vtkCellArray>::New();
     for(int i=0; i<nec; i++)
     {
         connectivity->InsertNextCell(nen);
         for(int j=0; j<nen; j++)
             connectivity->InsertCellPoint(mesh->getElem(i)->getLConn(j));
     }

     // Scalar property
     vtkSmartPointer<vtkDoubleArray> scalar = vtkSmartPointer<vtkDoubleArray>::New();
     scalar->SetName("Temperature");
     for(int i=0; i<nnl; i++)
         scalar->InsertNextValue(TL[i]);

     // Previously collected data which are outputPoints, outputCells, scalarProperty, are written to
     // vtkUnstructuredGrid type grid.
     vtkSmartPointer<vtkUnstructuredGrid> unsGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();
     unsGrid->SetPoints(outputPoints);
     unsGrid->SetCells(5,connectivity);
     unsGrid->GetPointData()->SetScalars(scalar);

     //Whatever collected in unstructured grid above is written to the "Title_mype.vtu" file below.
     vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
     dummy = settings->getTitle();
     dummy.append("_");
     dummy.append(strMype);
     dummy.append(".vtu");
     cout << dummy << endl;
     writer->SetFileName(dummy.c_str());
     writer->SetInputData(unsGrid);
     writer->Write();

     // Now we write the "Title.pvtu" file which contains the informtaiton about other files.
     if(mype == 0)
     {
         vtkSmartPointer<vtkXMLPUnstructuredGridWriter> pwriter =
             vtkSmartPointer<vtkXMLPUnstructuredGridWriter>::New();
         dummy = settings->getTitle();
         dummy.append(".pvtu");
         pwriter->SetFileName(dummy.c_str());
         pwriter->SetNumberOfPieces(npes);
         #if VTK_MAJOR_VERSION <= 5
             pwriter->SetInput(unsGrid);
         #else
             pwriter->SetInputData(unsGrid);
         #endif
         pwriter->Write();
     }

     return;
 }
