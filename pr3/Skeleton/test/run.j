#!/usr/bin/env zsh

### Job name
#BSUB -J para-pr3

### Job output (use %J  for job ID)
#BSUB -o para-pr3.%J.qout

### Request the time you need for execution in minutes
#BSUB -W 0:30

# Use the job queue that has been created for this particular lecture
#BSUB -P lect0027

### Request memory you need for your job in TOTAL in MB
#BSUB -M 512

### Use nodes exclusive
#BSUB -x

### Request the number of compute slots you want to use
#BSUB -n 64

### Use esub for OpenMP/shared memory jobs
#BSUB -a openmpi

### Change to the work directory
###cd <path to work directory>

### Execute your application
$MPIEXEC $FLAGS_MPI_BATCH ../build/2d_Unsteady
