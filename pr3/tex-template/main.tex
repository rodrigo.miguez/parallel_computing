\documentclass[a4paper, 11pt, oneside]{scrartcl}
\usepackage{lpp}
\usepackage{verbatim}
\usepackage[section]{placeins}

\begin{document}

\mtitle{Project 3}

\sauthor{Rodrigo Barros Miguez}

\maff{Laboratory for Parallel Programming}{rodrigo.miguez@rwth-aachen.de}{}

\section*{Introduction}

The utilization of MPI parallelism for accelerating numerical codes is a must when performing simulations of large data sets. MPI is used to parallelize codes designed for solving problems such as molecular dynamics, fluid dynamics, solid mechanics problems and the list goes on.\\
In this project, a heat diffusion problem is solved using a Galerkin finite element method. This solver is parallelized using one-sided MPI communication. The most important steps for the parallelization are shown and the timing results as well as the validation of the solver for a number of up to 64 ranks are provided.

\section*{Implementation and Results}

As described above, in this project the Galerkin finite element method which is used to solve the heat diffusion problem in project 1 is again visited. This time, however, the aim is to used one-sided MPI parallelization to be able use our code for a higher number of ranks.\\
The parallelization of the solver was done on top of a code provided to the students and it has three important parts: partition of elements and nodes, localization of element data, and completion of the parallelized functions (accumulateMass, localizeTemperature, and accumulateMTnew).\\
The code shown below represent the partition of elements and nodes. In this implementation, the both number of elements (ne) and the number of nodes (nn) are divided among the number of ranks such that the last rank in the program receives the remainder number of elements and/or nodes. This way, the if the ratio between the size of ne and nn and the number of ranks is not exact, the last rank will receive a smaller number of nodes and elements data.

\begin{verbatim}

  ...

    // Determine nec, mec
	// TODO
	if ( ( ne % npes ) == 0 ) {
	  nec = ne/npes;
	  mec = nec;
	}
	else {
	  nec = ( ne / npes ) + 1;
	  mec = nec;
	  if ( mype == npes - 1) {
	    nec = ne - ( nec * (npes - 1) );
	  }
	}

	// Determine nnc, mnc
	// TODO
	if ( ( nn % npes ) == 0 ) {
	  nnc = nn/npes;
	  mnc = nnc;
	}
	else {
	  nnc = ( nn / npes ) + 1;
	  mnc = nnc;
	  if ( mype == npes - 1) {
	    nnc = nn - ( nnc * (npes - 1) );
	  }
	}

  ...
\end{verbatim}

The next part, localization of element data, is done by following the algorithm provided in the class slides Lecture 19 page 9. In this algorithm, a window is created using \texttt{MPI\_Win\_create} with respect to the global variable xyz. Then, the \texttt{MPI\_Get} operation is used to retrieve the information from the global variable to the new local variable that each rank will possess.

\begin{verbatim}

  ...
  
void triMesh::localizeNodeCoordinates()
{
int mype, npes, in, inc, ipe;
MPI_Comm_rank(MPI_COMM_WORLD, &mype);
MPI_Comm_size(MPI_COMM_WORLD, &npes);

double CoordGetx[nnl];
double CoordGety[nnl];

// MPI Communication
MPI_Win win;
MPI_Win_create(xyz, nsd*nnc*sizeof(double), sizeof(double),
               MPI_INFO_NULL, MPI_COMM_WORLD, &win);
MPI_Win_fence(0, win);

for (int i = 0; i < nnl; i++) {
in = getNodeLToG()[i];
ipe = in / mnc;
inc = (in%mnc);
MPI_Get(&CoordGetx[i], 1, MPI_DOUBLE, ipe, nsd*inc, 1, MPI_DOUBLE, win);
MPI_Get(&CoordGety[i], 1, MPI_DOUBLE, ipe, nsd*inc+1, 1, MPI_DOUBLE, win);
}

MPI_Win_fence(0, win);
MPI_Win_free(&win);

for(int i=0; i<nnl; i++)
{
lNode[i].setX(CoordGetx[i]);
lNode[i].setY(CoordGety[i]);
}

return;
}

  ...
\end{verbatim}

Finally, the three MPI functions implemented in solver.cpp are shown below. Here again we used the algorithm provided in the lecture material. This time the \texttt{MPI\_Accumulate} was used instead of \texttt{MPI\_Put} because we need to sum the local variables into the global variable, so \texttt{MPI\_Accumulate} do the job using "\texttt{MPI\_SUM}" as the operation to be performed. This was done both for the assembly of the global variables massG and MTnewG. The temperature localization, however, followed a similar principle used for the coordinate localization described above but with only one \texttt{MPI\_Get} since we are working only with the temperature this time and with other parameters of the functional call changed.

\begin{verbatim}

...

void femSolver::accumulateMass()
{
int mype, npes, in, ipe, inc;
MPI_Comm_rank(MPI_COMM_WORLD, &mype);
MPI_Comm_size(MPI_COMM_WORLD, &npes);

int const nnl = mesh->getNnl();
int const mnc = mesh->getMnc();
int const nnc = mesh->getNnc();
double * massG = mesh->getMassG();
double * massL = new double [nnl];

for (int i=0; i < nnl; i++) {
massL[i] = mesh->getLNode(i)->getMass();
}

MPI_Win win;
MPI_Win_create(massG, nnc*sizeof(double), sizeof(double), MPI_INFO_NULL,
MPI_COMM_WORLD, &win);
MPI_Win_fence(0, win);

for (int i=0; i < nnl; i++) {
in = mesh->getNodeLToG()[i];
ipe = in / mnc;
inc = (in%mnc);
MPI_Accumulate(&massL[i], 1, MPI_DOUBLE, ipe, inc, 1, MPI_DOUBLE, MPI_SUM,
win);
}

MPI_Win_fence(0, win);
MPI_Win_free(&win);

return;
}

...

void femSolver::accumulateMTnew()
{
int mype, npes, in, ipe, inc;
MPI_Comm_rank(MPI_COMM_WORLD, &mype);
MPI_Comm_size(MPI_COMM_WORLD, &npes);

double * MTnewG = mesh->getMTnewG();
double * MTnewL = mesh->getMTnewL();
int const mnc = mesh->getMnc();
int const nnc = mesh->getNnc();
int const nnl = mesh->getNnl();

MPI_Win win;
MPI_Win_create(MTnewG, nnc*sizeof(double), sizeof(double), MPI_INFO_NULL,
MPI_COMM_WORLD, &win);
MPI_Win_fence(0, win);

for (int i=0; i < nnl; i++) {
in = mesh->getNodeLToG()[i];
ipe = in / mnc;
inc = (in%mnc);
MPI_Accumulate(&MTnewL[i], 1, MPI_DOUBLE, ipe, inc, 1, MPI_DOUBLE, MPI_SUM,
win);
}

MPI_Win_fence(0, win);
MPI_Win_free(&win);

return;
}

...

void femSolver::localizeTemperature()
{
int mype, npes, in, inc, ipe;                MPI_Comm_rank(MPI_COMM_WORLD, &mype);
MPI_Comm_size(MPI_COMM_WORLD, &npes);

int const mnc = mesh->getMnc();
int const nnc = mesh->getNnc();
int const nnl = mesh->getNnl();
double * TG = mesh->getTG();
double * TL = mesh->getTL();

MPI_Win win;
MPI_Win_create(TG, nnc*sizeof(double), sizeof(double), MPI_INFO_NULL, MPI_COMM_WORLD, &win);
MPI_Win_fence(0, win);

for (int i=0; i < nnl; i++) {
in = mesh->getNodeLToG()[i];
ipe = in / mnc;
inc = (in%mnc);
MPI_Get(&TL[i], 1, MPI_DOUBLE, ipe, inc, 1, MPI_DOUBLE, win);
}

MPI_Win_fence(0, win);
MPI_Win_free(&win);

return;
}

\end{verbatim}

The temperature distribution profile for this simulation is depicted below in Fig. 1, where it represents the solution for a number of 64 ranks. It is important to notice that the solution for a different number of ranks is the same, and 64 ranks was used in this case to show that the correct solution is obtained independently of the number of ranks utilized. In Fig. 2 we can see the rms error obtained for this run.\\
In addition to that result, the numerical and the analytical solutions are compared in Fig. 3, just like was done in project 1. It was observed that for this run, the final L2 norm error was equal to 0.0126620825511. The code used to obtain the results is showed below and the .csv file with the temperature distribution can be found in the additional files attached to this report.

\begin{verbatim}

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import csv

# **************************************************
# Get radial temperature from numerical solution
# **************************************************

# Get the number of nodes from .csv file and allocate memory
nlines = len(pd.read_csv('tempDist_n64.csv')) + 1
Ts = np.zeros([nlines])
r = np.zeros([nlines])

# Read temperature profile data
tempProfCSV =  csv.reader(open('tempDist_n64.csv'),quoting=csv.QUOTE_NONNUMERIC)

# Save solution values into r and Ts (numerical temperature obtained from solver)
indx = 0
for line in tempProfCSV:
Ts[indx] = line[0]
r[indx] = line[1]
indx = indx + 1

# **************************************************
# Get Error_rms/
# **************************************************

# Get the number of iterations from .csv file and allocate memory
nlines = len(pd.read_csv('l2error_n64.csv')) + 1
ite = np.zeros([nlines])
err = np.zeros([nlines])

# Read error rms data
errorRmsCSV =  csv.reader(open('l2error_n64.csv'),quoting=csv.QUOTE_NONNUMERIC)

# Save solution values into r and Ts (numerical temperature obtained from solver)
indx = 0
for line in errorRmsCSV:
ite[indx] = line[0]
err[indx] = line[1]
indx = indx + 1

# **************************************************
# Calculation of Te (analytical temperature)
# **************************************************

# Definition of variables
Ti = -100
Ta = 100
ri = 0.2
ra = 1

# Calculate Te
Te = Ti - ( np.log(r) - np.log(ri) ) * (( Ti - Ta ) / ( np.log(ra) - np.log(ri) ))

# **************************************************
# L2 NORM calculation
# **************************************************

L2sum=np.sum(np.mean((Te-Ts)**2))
L2norm = np.sqrt(L2sum)
print(L2norm)

# **************************************************
# Plot of the results
# **************************************************

plt.figure(1)
plt.plot(r, Te, 'r', label='Exact Solution')
plt.plot(r, Ts, 'b+', label='Numerical Solution')

plt.xlabel('X')
plt.ylabel('Temperature')
plt.grid(True)
plt.legend()
plt.savefig("Tcomp.png")

plt.figure(2)
plt.semilogy(ite, err, 'b')

plt.xlabel('Iteration')
plt.ylabel('Error rms')
plt.grid(True)
plt.savefig("ErRms.png")
plt.show()

return;
}

\end{verbatim}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{../images/para_tempDist_n64.png}
	\caption{Temperature distribution plot.}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{../scripts/ErRms.png}
	\caption{RMS error.}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{../scripts/Tcomp.png}
	\caption{Temperature distributions in the radial direction for numerical (64 ranks) and exact solution.}
\end{figure}

In addition, plots of the time measurements for the operations during the computation of one iteration in the solver is provided in Figs from 3 to 10. With these plots we can see that the timings for the MPI operations, or communication time, grows as the number of ranks utilized is higher. Additionally, the computation time decreases as the number of ranks is increased up until 8, when it starts to increase again.\\
The total time of each run for the different number of ranks is presented in Fig. 11, where we can see that the optimal number of ranks for size problem size sits around 8, which is in agreement to the vampir timing measurements. Larger than that, it is observed that the communication time increases a lot in comparison to the gains in the computation using ranks; thus, the timing using 64 ranks is higher than using 8 ranks.\\

\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{../images/para_n1.png}
\caption{Timing pattern for 1 rank.}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{../images/para_n2.png}
\caption{Timing pattern for 2 ranks.}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{../images/para_n4.png}
	\caption{Timing pattern for 4 ranks.}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{../images/para_n8.png}
	\caption{Timing pattern for 8 ranks.}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{../images/para_n16.png}
	\caption{Timing pattern for 16 ranks.}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{../images/para_n32.png}
	\caption{Timing pattern for 32 ranks.}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{../images/para_n64.png}
	\caption{Timing pattern for 64 ranks.}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{../images/runTimes.png}
	\caption{Run times for a increasing number of ranks.}
\end{figure}

\section*{Conclusion}

It was observed that the MPI library can accelerate the solver. However, the solution time gain was obtained for a number of rank up to 8. Above that there was an increase in the solution time, though the timings up to 64 ranks showed to be lower in comparison to the run using only 1 rank.\\
Vampir was used to get the timings for the most important operations during the solution. For that was possible to see that the communication time increases when increasing the number of ranks and the computation time decreases up until 8 ranks and then it starts to increase again. This shows why the 8 ranks solution presented the best timing among all number of ranks used to evaluate the code.\\
Finally, a comparison between the numerical solution using 64 ranks and the analytical solution was done where the L2 norm between the temperature results in the radial direction was as low as 0.0125, showing a good agreement between the two and validating the solver.

\bibliographystyle{}
\bibliography{}

\end{document}
