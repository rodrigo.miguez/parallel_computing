import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import csv

# **************************************************
# Get radial temperature from numerical solution
# **************************************************

# Get the number of nodes from .csv file and allocate memory
nlines = len(pd.read_csv('temperatureProfileX.csv')) + 1
Ts = np.zeros([nlines])
r = np.zeros([nlines])

# Read temperature profile data
tempProfCSV =  csv.reader(open('temperatureProfileX.csv'),quoting=csv.QUOTE_NONNUMERIC)

# Save solution values into r and Ts (numerical temperature obtained from solver)
indx = 0
for line in tempProfCSV:
    Ts[indx] = line[0]
    r[indx] = line[1]
    indx = indx + 1

# **************************************************
# Get Error_rms/
# **************************************************

# Get the number of iterations from .csv file and allocate memory
nlines = len(pd.read_csv('errorRms.csv')) + 1
ite = np.zeros([nlines])
err = np.zeros([nlines])

# Read error rms data
errorRmsCSV =  csv.reader(open('errorRms.csv'),quoting=csv.QUOTE_NONNUMERIC)

# Save solution values into r and Ts (numerical temperature obtained from solver)
indx = 0
for line in errorRmsCSV:
    ite[indx] = line[0]
    err[indx] = line[1]
    indx = indx + 1


# **************************************************
# Calculation of Te (analytical temperature)
# **************************************************

# Definition of variables
Ti = -100
Ta = 100
ri = 0.2
ra = 1

# Calculate Te
Te = Ti - ( np.log(r) - np.log(ri) ) * (( Ti - Ta ) / ( np.log(ra) - np.log(ri) ))

# **************************************************
# L2 NORM calculation
# **************************************************

L2sum=np.sum(np.mean((Te-Ts)**2))
L2norm = np.sqrt(L2sum)
print(L2norm)

# **************************************************
# Plot of the results
# **************************************************

plt.figure(1)
plt.plot(r, Te, 'r', label='Exact Solution')
plt.plot(r, Ts, 'b+', label='Numerical Solution')

plt.xlabel('X')
plt.ylabel('Temperature')
plt.grid(True)
plt.legend()
plt.savefig("Tcomp.png")

plt.figure(2)
plt.semilogy(ite, err, 'b')

plt.xlabel('Iteration')
plt.ylabel('Error rms')
plt.grid(True)
plt.savefig("ErRms.png")
plt.show()
