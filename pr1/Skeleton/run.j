#!/usr/bin/env bash

#BSUB -J para-pr1 # job name
#BSUB -o run%J.qout # job output (use %J for job ID)
#BSUB -W 00:30 # limits in hours:minutes
#BSUB -M 512 # memory in MB
#BSUB -P lect0027 # Use the job queue that has been created
                 # for this particular lecture.
                 # In case of permission problems, drop this line.
## For the future:
#  #BSUB -a openmpi -n 8 # MPI environment with 8 cores

# run the process
cd test/
../build/2d_Unsteady
