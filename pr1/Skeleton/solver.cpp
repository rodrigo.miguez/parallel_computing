#include "solver.h"

/***************************************************************************************************
solverControl
****************************************************************************************************
Flow control to prepare and solve the system.
***************************************************************************************************/
void femSolver::solverControl(inputSettings* argSettings, triMesh* argMesh)
{
    cout << endl << "=================== SOLUTION =====================" << endl;

    mesh = argMesh;
    settings = argSettings;
    int ne = mesh->getNe();

    //extern double testAr[ne*nen];

    for(int iec=0; iec<ne; iec++)
    {
        calculateJacobian(iec);
        calculateElementMatrices(iec);
    }
    applyDrichletBC();
    accumulateMass();
    explicitSolver();

    return;
}

/***************************************************************************************************
calculateJacobian
****************************************************************************************************
Compute and store the jacobian for each element.
***************************************************************************************************/
void femSolver::calculateJacobian(const int e)
{
    int myNode;     // node number for the current node
    double x[nen];  // x values for all the nodes of an element
    double y[nen];  // y values for all the nodes of an element

    triMasterElement* ME = mesh->getME(0);  // for easy access to the master element
    double * xyz = mesh->getXyz();

    double J[2][2];     // Jacobian for the current element
    double detJ;        // Jacobian determinant for the current element
    double invJ[2][2];  // inverse of Jacobian for the current element
    double dSdX[3];     // dSdx on a GQ point
    double dSdY[3];     // dSdy on a GQ point

    // collect element node coordinates in x[3] and y[3] matrices
    for (int i=0; i<nen; i++)
    {
        myNode =  mesh->getElem(e)->getConn(i);
        x[i] = xyz[myNode*nsd+xsd];
        y[i] = xyz[myNode*nsd+ysd];
    }

    // for all GQ points detJ, dSDx[3] and dSdY[3] are determined.
    for (int p=0; p<nGQP; p++)
    {
        // Calculate Jacobian
        J[0][0] = ME[p].getDSdKsi(0)*x[0] + ME[p].getDSdKsi(1)*x[1] + ME[p].getDSdKsi(2)*x[2];
        J[0][1] = ME[p].getDSdKsi(0)*y[0] + ME[p].getDSdKsi(1)*y[1] + ME[p].getDSdKsi(2)*y[2];
        J[1][0] = ME[p].getDSdEta(0)*x[0] + ME[p].getDSdEta(1)*x[1] + ME[p].getDSdEta(2)*x[2];
        J[1][1] = ME[p].getDSdEta(0)*y[0] + ME[p].getDSdEta(1)*y[1] + ME[p].getDSdEta(2)*y[2];

        //Calculate determinant of Jacobian and store in mesh
        detJ = J[0][0]*J[1][1] - J[0][1]*J[1][0];
        mesh->getElem(e)->setDetJ(p, detJ);

        // Calculate inverse of Jacobian
        invJ[0][0] =  J[1][1]/detJ;
        invJ[0][1] = -J[0][1]/detJ;
        invJ[1][0] = -J[1][0]/detJ;
        invJ[1][1] =  J[0][0]/detJ;

        // Calculate dSdx and dSdy and store in mesh
        dSdX[0] = invJ[0][0]*ME[p].getDSdKsi(0) + invJ[0][1]*ME[p].getDSdEta(0);
        dSdX[1] = invJ[0][0]*ME[p].getDSdKsi(1) + invJ[0][1]*ME[p].getDSdEta(1);
        dSdX[2] = invJ[0][0]*ME[p].getDSdKsi(2) + invJ[0][1]*ME[p].getDSdEta(2);
        dSdY[0] = invJ[1][0]*ME[p].getDSdKsi(0) + invJ[1][1]*ME[p].getDSdEta(0);
        dSdY[1] = invJ[1][0]*ME[p].getDSdKsi(1) + invJ[1][1]*ME[p].getDSdEta(1);
        dSdY[2] = invJ[1][0]*ME[p].getDSdKsi(2) + invJ[1][1]*ME[p].getDSdEta(2);

        mesh->getElem(e)->setDSdX(p, 0, dSdX[0]);
        mesh->getElem(e)->setDSdX(p, 1, dSdX[1]);
        mesh->getElem(e)->setDSdX(p, 2, dSdX[2]);
        mesh->getElem(e)->setDSdY(p, 0, dSdY[0]);
        mesh->getElem(e)->setDSdY(p, 1, dSdY[1]);
        mesh->getElem(e)->setDSdY(p, 2, dSdY[2]);
    }

    return;
}

/***************************************************************************************************
void femSolver::calculateElementMatrices(const int e)
****************************************************************************************************
Compute the K, M and F matrices. Then accumulate the total mass into the node structure.
***************************************************************************************************/
void femSolver::calculateElementMatrices(const int e)
{
    // Add code here

    double Ke[nen][nen];  // k values for all elements
    double Me[nen][nen];  // M values for all elements
    int p, i, j;

    for (i=0;i<nen;i++) {
      for (j=0;j<nen;j++) {
        Me[i][j]=0;
        Ke[i][j]=0;
      }
    }

    for (p=0;p<nGQP;p++) {
      for (i=0;i<nen;i++) {
        for (j=0;j<nen;j++) {

          Me[i][j] += ( mesh->getME(p)->getWeight() ) * ( mesh->getME(p)->getS(i) ) * \
            ( mesh->getME(p)->getS(j) ) * ( mesh->getElem(e)->getDetJ(p) );

          Ke[i][j] += ( settings->getD() ) * ( mesh->getME(p)->getWeight() ) * \
            ( mesh->getElem(e)->getDetJ(p) ) * ( ( mesh->getElem(e)->getDSdX(p,i) * \
              mesh->getElem(e)->getDSdX(p,j) ) + ( mesh->getElem(e)->getDSdY(p,i) * \
              mesh->getElem(e)->getDSdY(p,j) ) );

        }
      }
    }

    // MASS LUMPING
    double Mc[nen];
    double sumMc=0;
    double Ml[nen][nen];
    double Mee=0;

    for (i=0;i<nen;i++) {
      Mc[i]=0;
      for (j=0;j<nen;j++) {
        Ml[i][j]=0;
      }
    }

    for (i=0;i<nen;i++) {
        Mc[i]  = Me[i][i];
      for (j=0;j<nen;j++) {
        if ( i == j ) {
          sumMc += Me[i][j];
        }
        Mee += Me[i][j];
      }
    }

    for (i=0;i<nen;i++) {
      Ml[i][i] = ( Mc[i] * Mee ) / sumMc ;
    }

    // SAVING VARIABLES
    int nn=mesh->getNn();
    int myNode;

     for (i=0;i<nen;i++) {
       mesh->getElem(e)->setM(i, Ml[i][i]);
       for (j=0;j<nen;j++) {
         mesh->getElem(e)->setK(i,j,Ke[i][j]);
       }
     }

      for (j=0;j<nen;j++) {
        myNode =  mesh->getElem(e)->getConn(j);
        mesh->getNode(myNode)->addMass(Ml[j][j]);
      }

    return;
}

/***************************************************************************************************
void femSolver::applyDrichletBC()
****************************************************************************************************
if any of the boundary conditions set to Drichlet type
    visits all partition level nodes
        determines if any the nodes is on any of the side surfaces

***************************************************************************************************/
void femSolver::applyDrichletBC()
{
    // Add code here

    // STEP 1
    int myNode;     // node number for the current node
    double x;  // x values for all the nodes of an element
    double y;  // y values for all the nodes of an element

    triMasterElement* ME = mesh->getME(0);  // for easy access to the master element
    double * xyz = mesh->getXyz();

    double J[2][2];     // Jacobian for the current element
    double detJ;        // Jacobian determinant for the current element
    double invJ[2][2];  // inverse of Jacobian for the current element
    double dSdX[3];     // dSdx on a GQ point
    double dSdY[3];     // dSdy on a GQ point

    int ne=mesh->getNe();
    int nn=mesh->getNn();

    double * T=mesh->getT();
    double tol=4.0e-4;
    double iradSqrt=0.04;
    double oradSqrt=1.0;
    int k=0;
    int temp=0;
    for (int i=0; i<ne; i++) {
      for (int j=0; j<nen; j++) {
        myNode =  mesh->getElem(i)->getConn(j);
        x = xyz[myNode*nsd+xsd];
        y = xyz[myNode*nsd+ysd];

        if ( (x*x + y*y) < (iradSqrt + tol) ) {
          mesh->getNode(myNode)->setBCtype(1);
          T[myNode]=settings->getBC(1)->getValue1();
        }

        if( (x*x + y*y) > oradSqrt -tol ) {
          mesh->getNode(myNode)->setBCtype(1);
          T[myNode]=settings->getBC(2)->getValue1();
        }

      }
    }

    return;
}

/***************************************************************************************************
* void femSolver::explicitSolver()
***************************************************************************************************
*
**************************************************************************************************/
void femSolver::explicitSolver()
{

    // Add code here
    double * T=mesh->getT();
    int ne=mesh->getNe();
    int nn=mesh->getNn();
    double * M;
    double * K;
    double tstep=settings->getDt();
    int myNode;
    double * MTnew = mesh->getMTnew();
    int idt0, idt1;
    double * massG=mesh->getMassG();
    double errL;
    double err;
    double errInit;
    double errScaled;
    int ite;
    double Tnew[nn];
    ofstream outputFile("errorRms.csv");
    for (int ite=0;ite<settings->getNIter();ite++) {
      for (int i=0; i<ne; i++) {
        M=mesh->getElem(i)->getMptr();
        K=mesh->getElem(i)->getKptr();
        for (int n=0; n<nen; n++) {
          myNode =  mesh->getElem(i)->getConn(n);
          if (n == 0) {
            idt0 = mesh->getElem(i)->getConn(1);
            idt1 = mesh->getElem(i)->getConn(2);
            MTnew[myNode] += ((M[0] - tstep * (K[0])) * T[myNode]) + (( - tstep * (K[1])) * T[idt0]) + \
              (( - tstep * (K[2])) * T[idt1]);
          }
          if (n == 1) {
            idt0 = mesh->getElem(i)->getConn(0);
            idt1 = mesh->getElem(i)->getConn(2);
            MTnew[myNode] += (( - tstep * (K[3])) * T[idt0]) + ((M[1] - tstep * (K[4])) * T[myNode]) + \
              (( - tstep * (K[5])) * T[idt1]);
          }
          if (n == 2) {
            idt0 = mesh->getElem(i)->getConn(0);
            idt1 = mesh->getElem(i)->getConn(1);
            MTnew[myNode] += (( - tstep * (K[6])) * T[idt0]) + (( - tstep * (K[7])) * T[idt1]) + \
              ((M[2] - tstep * (K[8])) * T[myNode]);
          }
        }
      }
      errL = 0;
      for (int i=0; i<nn;i++) {
        if (mesh->getNode(i)->getBCtype() == 0) {
          Tnew[i]=MTnew[i]/massG[i];
          errL += pow(Tnew[i]-T[i],2.0);
          T[i]=Tnew[i];
          MTnew[i]=0;
        }
      }
      err=sqrt(errL/nn);
      if (ite == 0) {
        errInit=err;
      }
      errScaled=err/errInit;
      outputFile << ite << "," << errScaled << endl;
      if (errScaled < 1e-6) {
        cout <<"Iteration: " << ite << ", Error= " << errScaled << endl;
        break;
      }
    }

    return;

}

/***************************************************************************************************
* void femSolver::accumulateMass()
***************************************************************************************************
*
**************************************************************************************************/
void femSolver::accumulateMass()
{
    int nn = mesh->getNn();
    double * massG = mesh->getMassG();


    for(int i=0; i<nn; i++)
    {
        massG[i] = mesh->getNode(i)->getMass();
    }

    return;
}
